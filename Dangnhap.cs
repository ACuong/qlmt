﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace BTL.NET1
{
    public partial class Dangnhap : Form
    {
        public Dangnhap()
        {
            InitializeComponent();
   
        }

        private void button1_dangnhap_Click(object sender, EventArgs e)
        {
            SqlConnection con;
            string conString = @"Data Source=(localdb)\ProjectsV13;Initial Catalog=BTL.NET1;Integrated Security=True";
            con = new SqlConnection(conString);

            try
            {
                con.Open();
                string tendangnhap = textBox1_tendangnhap.Text;
                string matkhau = textBox2_matkhau.Text;
                string sql = "select *from Dangnhap where Tendangnhap = @Tendangnhap, Matkhau =@Matkhau";
                SqlCommand cmd = new SqlCommand(sql, con);
                SqlDataReader dr = cmd.ExecuteReader();
                if(dr.Read()==true)
                {
                    MessageBox.Show("Dang nhap thanh cong");
                }
                else
                {
                    MessageBox.Show("Sai mat khau va sai ten ");
                }
            }
            catch(Exception ex)
            {
                MessageBox.Show("Lỗi đăng nhập");
            }

        }

        private void button2_thoat_Click(object sender, EventArgs e)
        {
            DialogResult traloi;
            traloi = MessageBox.Show("Bạn có muốn thoát không ?", "Trả lời",
            MessageBoxButtons.OKCancel, MessageBoxIcon.Question);
            if (traloi == DialogResult.OK)
                Application.Exit();
        }
    }
}
