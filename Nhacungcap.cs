﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;
using System.Configuration;


namespace BTL.NET1
{
    public partial class Nhacungcap : Form
    {
   
        public Nhacungcap()
        {
            InitializeComponent();
            
        }

        SqlConnection con ;   //khoi tao doi tuong de ket noi 
        
        private void Nhacungcap_Load(object sender, EventArgs e)
        {

            string conString = @"Data Source=(localdb)\ProjectsV13;Initial Catalog=BTL.NET1;Integrated Security=True";
            con = new SqlConnection(conString);// tao doi tuong                           
            con.Open();
            Hienthi();
        }

        public void Hienthi()
        {
            string sqlSELECT = "SELECT *FROM Nhacungcap";
            SqlCommand cmd = new SqlCommand(sqlSELECT, con);  
            SqlDataReader dr = cmd.ExecuteReader();//de doc 
            DataTable dtNhacungcap = new DataTable();// tao ra doi tuong bang 
            dtNhacungcap.Load(dr);
            dataGridView_ncc.DataSource = dtNhacungcap;
        }
         
        private void button1_them_Click(object sender, EventArgs e)
        {
            string sqlINSERT = "INSERT INTO Nhacungcap VALUES(@Mancc,@Tenncc,@Sdt,@Diachi)";
            SqlCommand cmd = new SqlCommand(sqlINSERT, con);
            cmd.Parameters.AddWithValue("Mancc",textBox1_mancc.Text);
            cmd.Parameters.AddWithValue("Tenncc", textBox2_tenncc.Text);
            cmd.Parameters.AddWithValue("Sdt",textBox3_sdt.Text);
            cmd.Parameters.AddWithValue("Diachi", textBox4_diachi.Text);
            cmd.ExecuteNonQuery();
            Hienthi();
        }

        private void button2_sua_Click(object sender, EventArgs e)
        {
            string sqlEdit = "UPDATE Nhacungcap SET Tenncc =@Tenncc , Sdt = @Sdt , Diachi =@Diachi";//de sua duoc thi ncc phai ton tai trong sql va de ton tai no mk thong qua ma no de tim kiem de sua
            SqlCommand cmd = new SqlCommand(sqlEdit, con);
            cmd.Parameters.AddWithValue("Mancc", textBox1_mancc.Text);
            cmd.Parameters.AddWithValue("Tenncc", textBox2_tenncc.Text);
            cmd.Parameters.AddWithValue("Sdt", textBox3_sdt.Text);
            cmd.Parameters.AddWithValue("Diachi", textBox4_diachi.Text);
            cmd.ExecuteNonQuery();
            Hienthi();
        }

        private void button3_xoa_Click(object sender, EventArgs e)
        {
            string sqlDELETE = "DELETE FROM Nhacungcap WHERE Mancc =@Mancc";
            SqlCommand cmd = new SqlCommand(sqlDELETE, con);
            cmd.Parameters.AddWithValue("Mancc", textBox1_mancc.Text);
            cmd.Parameters.AddWithValue("Tenncc", textBox2_tenncc.Text);
            cmd.Parameters.AddWithValue("Sdt", textBox3_sdt.Text);
            cmd.Parameters.AddWithValue("Diachi", textBox4_diachi.Text);
            cmd.ExecuteNonQuery();
            Hienthi();
        }

        private void dataGridView_ncc_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }

        private void button1_thoat_Click(object sender, EventArgs e)
        {
            this.Close();
            Khachhang frm = new Khachhang();
            frm.Show();
        }

        private void Nhacungcap_FormClosing(object sender, FormClosingEventArgs e)
        {
            con.Close();
        }

        private void button_timkiem_Click(object sender, EventArgs e)
        {
            string sqlTmkiem = "SELECT *FROM Nhacungcap WHERE Mancc =@Mancc";
            SqlCommand cmd = new SqlCommand(sqlTmkiem, con);
            cmd.Parameters.AddWithValue("Mancc", textBox1.Text);
            cmd.Parameters.AddWithValue("Tenncc",textBox1.Text);
            cmd.Parameters.AddWithValue("Sdt", textBox1.Text);
            cmd.Parameters.AddWithValue("Diachi", textBox1.Text);
            cmd.ExecuteNonQuery();
            SqlDataReader dr = cmd.ExecuteReader();//de doc 
            DataTable dtNhacungcap = new DataTable();
            dtNhacungcap.Load(dr);
            dataGridView_ncc.DataSource = dtNhacungcap;
        }
    }
    }