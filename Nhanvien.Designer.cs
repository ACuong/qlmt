﻿
namespace BTL.NET1
{
    partial class Nhanvien
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.textBox_mnv = new System.Windows.Forms.TextBox();
            this.textBox_tnv = new System.Windows.Forms.TextBox();
            this.textBox_sdtnhanvien = new System.Windows.Forms.TextBox();
            this.textBox_diachinhanvien = new System.Windows.Forms.TextBox();
            this.button_themnhanvien = new System.Windows.Forms.Button();
            this.button_suanhanvien = new System.Windows.Forms.Button();
            this.button_xoanhanvien = new System.Windows.Forms.Button();
            this.button_thoatnhanvien = new System.Windows.Forms.Button();
            this.label6 = new System.Windows.Forms.Label();
            this.textBox5 = new System.Windows.Forms.TextBox();
            this.button_timkiemnhanvien = new System.Windows.Forms.Button();
            this.dataGridView_nhanvien = new System.Windows.Forms.DataGridView();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView_nhanvien)).BeginInit();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.Color.Blue;
            this.label1.Location = new System.Drawing.Point(251, 9);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(271, 25);
            this.label1.TabIndex = 0;
            this.label1.Text = "THÔNG TIN NHÂN VIÊN ";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(9, 58);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(110, 19);
            this.label2.TabIndex = 1;
            this.label2.Text = "Mã nhân viên :";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(7, 104);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(112, 19);
            this.label3.TabIndex = 2;
            this.label3.Text = "Tên nhân viên :";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(9, 150);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(104, 19);
            this.label4.TabIndex = 3;
            this.label4.Text = "Số điện thoại :";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(9, 198);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(65, 19);
            this.label5.TabIndex = 4;
            this.label5.Text = "Địa chỉ :";
            // 
            // textBox_mnv
            // 
            this.textBox_mnv.Location = new System.Drawing.Point(125, 56);
            this.textBox_mnv.Name = "textBox_mnv";
            this.textBox_mnv.Size = new System.Drawing.Size(145, 20);
            this.textBox_mnv.TabIndex = 5;
            // 
            // textBox_tnv
            // 
            this.textBox_tnv.Location = new System.Drawing.Point(125, 105);
            this.textBox_tnv.Name = "textBox_tnv";
            this.textBox_tnv.Size = new System.Drawing.Size(145, 20);
            this.textBox_tnv.TabIndex = 6;
            // 
            // textBox_sdtnhanvien
            // 
            this.textBox_sdtnhanvien.Location = new System.Drawing.Point(125, 151);
            this.textBox_sdtnhanvien.Name = "textBox_sdtnhanvien";
            this.textBox_sdtnhanvien.Size = new System.Drawing.Size(145, 20);
            this.textBox_sdtnhanvien.TabIndex = 7;
            // 
            // textBox_diachinhanvien
            // 
            this.textBox_diachinhanvien.Location = new System.Drawing.Point(125, 197);
            this.textBox_diachinhanvien.Name = "textBox_diachinhanvien";
            this.textBox_diachinhanvien.Size = new System.Drawing.Size(145, 20);
            this.textBox_diachinhanvien.TabIndex = 8;
            // 
            // button_themnhanvien
            // 
            this.button_themnhanvien.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(128)))), ((int)(((byte)(255)))));
            this.button_themnhanvien.ForeColor = System.Drawing.Color.Black;
            this.button_themnhanvien.Location = new System.Drawing.Point(310, 54);
            this.button_themnhanvien.Name = "button_themnhanvien";
            this.button_themnhanvien.Size = new System.Drawing.Size(75, 23);
            this.button_themnhanvien.TabIndex = 9;
            this.button_themnhanvien.Text = "Thêm";
            this.button_themnhanvien.UseVisualStyleBackColor = false;
            this.button_themnhanvien.Click += new System.EventHandler(this.button_themnhanvien_Click);
            // 
            // button_suanhanvien
            // 
            this.button_suanhanvien.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(128)))), ((int)(((byte)(255)))));
            this.button_suanhanvien.Location = new System.Drawing.Point(437, 53);
            this.button_suanhanvien.Name = "button_suanhanvien";
            this.button_suanhanvien.Size = new System.Drawing.Size(75, 23);
            this.button_suanhanvien.TabIndex = 10;
            this.button_suanhanvien.Text = "Sửa";
            this.button_suanhanvien.UseVisualStyleBackColor = false;
            this.button_suanhanvien.Click += new System.EventHandler(this.button_suanhanvien_Click);
            // 
            // button_xoanhanvien
            // 
            this.button_xoanhanvien.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(128)))), ((int)(((byte)(255)))));
            this.button_xoanhanvien.Location = new System.Drawing.Point(565, 53);
            this.button_xoanhanvien.Name = "button_xoanhanvien";
            this.button_xoanhanvien.Size = new System.Drawing.Size(75, 23);
            this.button_xoanhanvien.TabIndex = 11;
            this.button_xoanhanvien.Text = "Xóa";
            this.button_xoanhanvien.UseVisualStyleBackColor = false;
            this.button_xoanhanvien.Click += new System.EventHandler(this.button_xoanhanvien_Click);
            // 
            // button_thoatnhanvien
            // 
            this.button_thoatnhanvien.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(128)))), ((int)(((byte)(255)))));
            this.button_thoatnhanvien.Location = new System.Drawing.Point(696, 53);
            this.button_thoatnhanvien.Name = "button_thoatnhanvien";
            this.button_thoatnhanvien.Size = new System.Drawing.Size(75, 23);
            this.button_thoatnhanvien.TabIndex = 12;
            this.button_thoatnhanvien.Text = "Thoát";
            this.button_thoatnhanvien.UseVisualStyleBackColor = false;
            this.button_thoatnhanvien.Click += new System.EventHandler(this.button_thoatnhanvien_Click);
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Times New Roman", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(447, 105);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(170, 22);
            this.label6.TabIndex = 13;
            this.label6.Text = "Thông tin tìm kiếm ";
            // 
            // textBox5
            // 
            this.textBox5.Location = new System.Drawing.Point(469, 149);
            this.textBox5.Name = "textBox5";
            this.textBox5.Size = new System.Drawing.Size(120, 20);
            this.textBox5.TabIndex = 14;
            // 
            // button_timkiemnhanvien
            // 
            this.button_timkiemnhanvien.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(128)))), ((int)(((byte)(255)))));
            this.button_timkiemnhanvien.Location = new System.Drawing.Point(492, 194);
            this.button_timkiemnhanvien.Name = "button_timkiemnhanvien";
            this.button_timkiemnhanvien.Size = new System.Drawing.Size(75, 23);
            this.button_timkiemnhanvien.TabIndex = 15;
            this.button_timkiemnhanvien.Text = "Tìm kiếm ";
            this.button_timkiemnhanvien.UseVisualStyleBackColor = false;
            this.button_timkiemnhanvien.Click += new System.EventHandler(this.button_timkiemnhanvien_Click);
            // 
            // dataGridView_nhanvien
            // 
            this.dataGridView_nhanvien.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView_nhanvien.Location = new System.Drawing.Point(60, 254);
            this.dataGridView_nhanvien.Name = "dataGridView_nhanvien";
            this.dataGridView_nhanvien.Size = new System.Drawing.Size(643, 150);
            this.dataGridView_nhanvien.TabIndex = 16;
            this.dataGridView_nhanvien.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dataGridView_nhanvien_CellContentClick);
            // 
            // Nhanvien
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.dataGridView_nhanvien);
            this.Controls.Add(this.button_timkiemnhanvien);
            this.Controls.Add(this.textBox5);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.button_thoatnhanvien);
            this.Controls.Add(this.button_xoanhanvien);
            this.Controls.Add(this.button_suanhanvien);
            this.Controls.Add(this.button_themnhanvien);
            this.Controls.Add(this.textBox_diachinhanvien);
            this.Controls.Add(this.textBox_sdtnhanvien);
            this.Controls.Add(this.textBox_tnv);
            this.Controls.Add(this.textBox_mnv);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Name = "Nhanvien";
            this.Text = "Nhanvien";
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.Nhanvien_FormClosed);
            this.Load += new System.EventHandler(this.Nhanvien_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView_nhanvien)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox textBox_mnv;
        private System.Windows.Forms.TextBox textBox_tnv;
        private System.Windows.Forms.TextBox textBox_sdtnhanvien;
        private System.Windows.Forms.TextBox textBox_diachinhanvien;
        private System.Windows.Forms.Button button_themnhanvien;
        private System.Windows.Forms.Button button_suanhanvien;
        private System.Windows.Forms.Button button_xoanhanvien;
        private System.Windows.Forms.Button button_thoatnhanvien;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox textBox5;
        private System.Windows.Forms.Button button_timkiemnhanvien;
        private System.Windows.Forms.DataGridView dataGridView_nhanvien;
    }
}