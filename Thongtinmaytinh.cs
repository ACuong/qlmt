﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace BTL.NET1
{
    public partial class Thongtinmaytinh : Form
    {
        public Thongtinmaytinh()
        {
            InitializeComponent();
        }
        SqlConnection con;
        private void Thongtinmaytinh_Load(object sender, EventArgs e)
        {
            string conString = @"Data Source=(localdb)\ProjectsV13;Initial Catalog=BTL.NET1;Integrated Security=True";
            con = new SqlConnection(conString);// tao doi tuong                           
            con.Open();
            Hienthi();
        }
        public void Hienthi()
        {
            string sqlSELECT = "SELECT *FROM Thongtinmaytinh";
            SqlCommand cmd = new SqlCommand(sqlSELECT, con);
            SqlDataReader dr = cmd.ExecuteReader();//de doc 
            DataTable dt = new DataTable();// tao ra doi tuong bang 
            dt.Load(dr);
            dataGridView_ttmt.DataSource = dt;
            comboBox_ncc_ttmt.ValueMember = "MaNCC";
           comboBox_ncc_ttmt.DisplayMember = "MaNCC"; 
            
        }

        private void button_themttmt_Click(object sender, EventArgs e)
        {
            string sqlINSERT = "INSERT Thongtinmaytinh VALUES(@Mamt,@Tenmt,@Mancc,@Dongia,@Soluong)";
            SqlCommand cmd = new SqlCommand(sqlINSERT, con);
            cmd.Parameters.AddWithValue("Mamt", textBox_mmt.Text);
            cmd.Parameters.AddWithValue("Tenmt", textBox_tmt.Text);
            cmd.Parameters.AddWithValue("Mancc", comboBox_ncc_ttmt.Text);
            cmd.Parameters.AddWithValue("Soluong", textBox_soluongmt.Text);
            cmd.Parameters.AddWithValue("Dongia", textBox_dongiamt.Text);
            
            cmd.ExecuteNonQuery();
            Hienthi();
        }

        private void button_luuttmt_Click(object sender, EventArgs e)
        {
            string sqlSave = "SELECT *FROM Thongtinmaytinh";
            SqlCommand cmd = new SqlCommand(sqlSave, con);
            cmd.Parameters.AddWithValue("Mamt", textBox_mmt.Text);
            cmd.Parameters.AddWithValue("Tenmt", textBox_tmt.Text);
            cmd.Parameters.AddWithValue("Mancc", comboBox_ncc_ttmt.Text);
            cmd.Parameters.AddWithValue("Soluong", textBox_soluongmt.Text);
            cmd.Parameters.AddWithValue("Dongia", textBox_dongiamt.Text);
            
            cmd.ExecuteNonQuery();
            Hienthi();
        }

        private void button_suattmt_Click(object sender, EventArgs e)
        {
            string sqlEdit = "UPDATE Thongtinmaytinh SET Tenmt =@Tenmt , Soluong = @Soluong , Dongia =@Dongia";//de sua duoc thi kh phai ton tai trong sql va de ton tai no mk thong qua ma no de tim kiem de sua
            SqlCommand cmd = new SqlCommand(sqlEdit, con);
            cmd.Parameters.AddWithValue("Mamt", textBox_mmt.Text);
            cmd.Parameters.AddWithValue("Tenmt", textBox_tmt.Text);
            cmd.Parameters.AddWithValue("Mancc", comboBox_ncc_ttmt.Text);
            cmd.Parameters.AddWithValue("Soluong", textBox_soluongmt.Text);
            cmd.Parameters.AddWithValue("Dongia", textBox_dongiamt.Text);
            cmd.ExecuteNonQuery();
            Hienthi();
        }

        private void button_xoattmt_Click(object sender, EventArgs e)
        {
            string sqlDELETE = "DELETE FROM Thongtinmaytinh WHERE Mamt =@Mamt , Tenmt =@Tenmt , Soluong =@Soluong , Dongia =@Dongia";
            SqlCommand cmd = new SqlCommand(sqlDELETE, con);
            cmd.Parameters.AddWithValue("Mamt", textBox_mmt.Text);
            cmd.Parameters.AddWithValue("Tenmt", textBox_tmt.Text);
            cmd.Parameters.AddWithValue("Mancc", comboBox_ncc_ttmt.Text);
            cmd.Parameters.AddWithValue("Soluong", textBox_soluongmt.Text);
            cmd.Parameters.AddWithValue("Dongia", textBox_dongiamt.Text);
           
            Hienthi();
        }

        private void button_thoatttmt_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void button_timkiemttmt_Click(object sender, EventArgs e)
        {
            string sqlTmkiem = "SELECT *FROM Thongtinmaytinh WHERE Mamt =@Mamt";
            SqlCommand cmd = new SqlCommand(sqlTmkiem, con);
            cmd.Parameters.AddWithValue("Mamt", textBox_timkiemttmt.Text);
            cmd.Parameters.AddWithValue("Tenmt", textBox_timkiemttmt.Text);
            cmd.Parameters.AddWithValue("Mancc", textBox_timkiemttmt.Text);
            cmd.Parameters.AddWithValue("Soluong", textBox_timkiemttmt.Text);
            cmd.Parameters.AddWithValue("Dongia", textBox_timkiemttmt.Text);
            cmd.ExecuteNonQuery();
            SqlDataReader dr = cmd.ExecuteReader();//de doc 
            DataTable dt = new DataTable();
            dt.Load(dr);
            dataGridView_ttmt.DataSource = dt;
        }

        private void Thongtinmaytinh_FormClosed(object sender, FormClosedEventArgs e)
        {
            con.Close();
        }

        private void dataGridView_ttmt_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }
    }
}
