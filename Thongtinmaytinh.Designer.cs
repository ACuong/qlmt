﻿
namespace BTL.NET1
{
    partial class Thongtinmaytinh
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.textBox_mmt = new System.Windows.Forms.TextBox();
            this.textBox_tmt = new System.Windows.Forms.TextBox();
            this.textBox_dongiamt = new System.Windows.Forms.TextBox();
            this.textBox_soluongmt = new System.Windows.Forms.TextBox();
            this.button_themttmt = new System.Windows.Forms.Button();
            this.button_suattmt = new System.Windows.Forms.Button();
            this.button_xoattmt = new System.Windows.Forms.Button();
            this.button_thoatttmt = new System.Windows.Forms.Button();
            this.button_luuttmt = new System.Windows.Forms.Button();
            this.label7 = new System.Windows.Forms.Label();
            this.textBox_timkiemttmt = new System.Windows.Forms.TextBox();
            this.button_timkiemttmt = new System.Windows.Forms.Button();
            this.comboBox_ncc_ttmt = new System.Windows.Forms.ComboBox();
            this.dataGridView_ttmt = new System.Windows.Forms.DataGridView();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView_ttmt)).BeginInit();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Times New Roman", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.Color.Blue;
            this.label1.Location = new System.Drawing.Point(272, 9);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(226, 22);
            this.label1.TabIndex = 0;
            this.label1.Text = "THÔNG TIN MÁY TÍNH ";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.ForeColor = System.Drawing.Color.Black;
            this.label2.Location = new System.Drawing.Point(12, 54);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(103, 19);
            this.label2.TabIndex = 1;
            this.label2.Text = "Mã máy tính :";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(12, 98);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(105, 19);
            this.label3.TabIndex = 2;
            this.label3.Text = "Tên máy tính :";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(12, 140);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(136, 19);
            this.label4.TabIndex = 3;
            this.label4.Text = "Mã nhà cung cấp : ";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(340, 54);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(71, 19);
            this.label5.TabIndex = 4;
            this.label5.Text = "Đơn giá :";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(340, 97);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(78, 19);
            this.label6.TabIndex = 5;
            this.label6.Text = "Số lượng :";
            // 
            // textBox_mmt
            // 
            this.textBox_mmt.Location = new System.Drawing.Point(154, 53);
            this.textBox_mmt.Name = "textBox_mmt";
            this.textBox_mmt.Size = new System.Drawing.Size(133, 20);
            this.textBox_mmt.TabIndex = 6;
            // 
            // textBox_tmt
            // 
            this.textBox_tmt.Location = new System.Drawing.Point(154, 97);
            this.textBox_tmt.Name = "textBox_tmt";
            this.textBox_tmt.Size = new System.Drawing.Size(133, 20);
            this.textBox_tmt.TabIndex = 7;
            // 
            // textBox_dongiamt
            // 
            this.textBox_dongiamt.Location = new System.Drawing.Point(446, 53);
            this.textBox_dongiamt.Name = "textBox_dongiamt";
            this.textBox_dongiamt.Size = new System.Drawing.Size(160, 20);
            this.textBox_dongiamt.TabIndex = 9;
            // 
            // textBox_soluongmt
            // 
            this.textBox_soluongmt.Location = new System.Drawing.Point(446, 98);
            this.textBox_soluongmt.Name = "textBox_soluongmt";
            this.textBox_soluongmt.Size = new System.Drawing.Size(160, 20);
            this.textBox_soluongmt.TabIndex = 10;
            // 
            // button_themttmt
            // 
            this.button_themttmt.Location = new System.Drawing.Point(16, 185);
            this.button_themttmt.Name = "button_themttmt";
            this.button_themttmt.Size = new System.Drawing.Size(75, 23);
            this.button_themttmt.TabIndex = 11;
            this.button_themttmt.Text = "Thêm ";
            this.button_themttmt.UseVisualStyleBackColor = true;
            this.button_themttmt.Click += new System.EventHandler(this.button_themttmt_Click);
            // 
            // button_suattmt
            // 
            this.button_suattmt.Location = new System.Drawing.Point(212, 185);
            this.button_suattmt.Name = "button_suattmt";
            this.button_suattmt.Size = new System.Drawing.Size(75, 23);
            this.button_suattmt.TabIndex = 12;
            this.button_suattmt.Text = "Sửa";
            this.button_suattmt.UseVisualStyleBackColor = true;
            this.button_suattmt.Click += new System.EventHandler(this.button_suattmt_Click);
            // 
            // button_xoattmt
            // 
            this.button_xoattmt.Location = new System.Drawing.Point(315, 185);
            this.button_xoattmt.Name = "button_xoattmt";
            this.button_xoattmt.Size = new System.Drawing.Size(75, 23);
            this.button_xoattmt.TabIndex = 13;
            this.button_xoattmt.Text = "Xóa";
            this.button_xoattmt.UseVisualStyleBackColor = true;
            this.button_xoattmt.Click += new System.EventHandler(this.button_xoattmt_Click);
            // 
            // button_thoatttmt
            // 
            this.button_thoatttmt.Location = new System.Drawing.Point(423, 185);
            this.button_thoatttmt.Name = "button_thoatttmt";
            this.button_thoatttmt.Size = new System.Drawing.Size(75, 23);
            this.button_thoatttmt.TabIndex = 14;
            this.button_thoatttmt.Text = "Thoát";
            this.button_thoatttmt.UseVisualStyleBackColor = true;
            this.button_thoatttmt.Click += new System.EventHandler(this.button_thoatttmt_Click);
            // 
            // button_luuttmt
            // 
            this.button_luuttmt.Location = new System.Drawing.Point(112, 185);
            this.button_luuttmt.Name = "button_luuttmt";
            this.button_luuttmt.Size = new System.Drawing.Size(75, 23);
            this.button_luuttmt.TabIndex = 15;
            this.button_luuttmt.Text = "Lưu";
            this.button_luuttmt.UseVisualStyleBackColor = true;
            this.button_luuttmt.Click += new System.EventHandler(this.button_luuttmt_Click);
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.Location = new System.Drawing.Point(599, 138);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(138, 19);
            this.label7.TabIndex = 16;
            this.label7.Text = "Thông tin tìm kiếm ";
            // 
            // textBox_timkiemttmt
            // 
            this.textBox_timkiemttmt.Location = new System.Drawing.Point(617, 170);
            this.textBox_timkiemttmt.Name = "textBox_timkiemttmt";
            this.textBox_timkiemttmt.Size = new System.Drawing.Size(107, 20);
            this.textBox_timkiemttmt.TabIndex = 17;
            // 
            // button_timkiemttmt
            // 
            this.button_timkiemttmt.Location = new System.Drawing.Point(637, 208);
            this.button_timkiemttmt.Name = "button_timkiemttmt";
            this.button_timkiemttmt.Size = new System.Drawing.Size(75, 23);
            this.button_timkiemttmt.TabIndex = 18;
            this.button_timkiemttmt.Text = "Tìm kiếm ";
            this.button_timkiemttmt.UseVisualStyleBackColor = true;
            this.button_timkiemttmt.Click += new System.EventHandler(this.button_timkiemttmt_Click);
            // 
            // comboBox_ncc_ttmt
            // 
            this.comboBox_ncc_ttmt.FormattingEnabled = true;
            this.comboBox_ncc_ttmt.Location = new System.Drawing.Point(154, 136);
            this.comboBox_ncc_ttmt.Name = "comboBox_ncc_ttmt";
            this.comboBox_ncc_ttmt.Size = new System.Drawing.Size(133, 21);
            this.comboBox_ncc_ttmt.TabIndex = 19;
            // 
            // dataGridView_ttmt
            // 
            this.dataGridView_ttmt.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView_ttmt.Location = new System.Drawing.Point(16, 259);
            this.dataGridView_ttmt.Name = "dataGridView_ttmt";
            this.dataGridView_ttmt.Size = new System.Drawing.Size(590, 150);
            this.dataGridView_ttmt.TabIndex = 20;
            this.dataGridView_ttmt.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dataGridView_ttmt_CellContentClick);
            // 
            // Thongtinmaytinh
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.dataGridView_ttmt);
            this.Controls.Add(this.comboBox_ncc_ttmt);
            this.Controls.Add(this.button_timkiemttmt);
            this.Controls.Add(this.textBox_timkiemttmt);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.button_luuttmt);
            this.Controls.Add(this.button_thoatttmt);
            this.Controls.Add(this.button_xoattmt);
            this.Controls.Add(this.button_suattmt);
            this.Controls.Add(this.button_themttmt);
            this.Controls.Add(this.textBox_soluongmt);
            this.Controls.Add(this.textBox_dongiamt);
            this.Controls.Add(this.textBox_tmt);
            this.Controls.Add(this.textBox_mmt);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.ForeColor = System.Drawing.SystemColors.ControlText;
            this.Name = "Thongtinmaytinh";
            this.Text = "Thongtinmaytinh";
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.Thongtinmaytinh_FormClosed);
            this.Load += new System.EventHandler(this.Thongtinmaytinh_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView_ttmt)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox textBox_mmt;
        private System.Windows.Forms.TextBox textBox_tmt;
        private System.Windows.Forms.TextBox textBox_dongiamt;
        private System.Windows.Forms.TextBox textBox_soluongmt;
        private System.Windows.Forms.Button button_themttmt;
        private System.Windows.Forms.Button button_suattmt;
        private System.Windows.Forms.Button button_xoattmt;
        private System.Windows.Forms.Button button_thoatttmt;
        private System.Windows.Forms.Button button_luuttmt;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.TextBox textBox_timkiemttmt;
        private System.Windows.Forms.Button button_timkiemttmt;
        private System.Windows.Forms.ComboBox comboBox_ncc_ttmt;
        private System.Windows.Forms.DataGridView dataGridView_ttmt;
    }
}