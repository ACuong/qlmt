﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace BTL.NET1
{
    public partial class Nhanvien : Form
    {
        public Nhanvien()
        {
            InitializeComponent();
        }
        SqlConnection con;
        private void Nhanvien_Load(object sender, EventArgs e)
        {
            string conString = @"Data Source=(localdb)\ProjectsV13;Initial Catalog=BTL.NET1;Integrated Security=True";
            con = new SqlConnection(conString);// tao doi tuong                           
            con.Open();
            Hienthi();
        }

        public void Hienthi()
        {
            string sqlSELECT = "SELECT *FROM Nhanvien";
            SqlCommand cmd = new SqlCommand(sqlSELECT, con);
            SqlDataReader dr = cmd.ExecuteReader();//de doc 
            DataTable dt = new DataTable();// tao ra doi tuong bang 
            dt.Load(dr);
            dataGridView_nhanvien.DataSource = dt;
        }

        private void button_themnhanvien_Click(object sender, EventArgs e)
        {
            string sqlINSERT = "INSERT Nhanvien VALUES(@Manv,@Tennv,@Sdt,@Diachi)";
            SqlCommand cmd = new SqlCommand(sqlINSERT, con);
            cmd.Parameters.AddWithValue("Manv", textBox_mnv.Text);
            cmd.Parameters.AddWithValue("Tennv", textBox_tnv.Text);
            cmd.Parameters.AddWithValue("Sdt", textBox_sdtnhanvien.Text);
            cmd.Parameters.AddWithValue("Diachi", textBox_diachinhanvien.Text);
            cmd.ExecuteNonQuery();
            Hienthi();
        }

        private void button_suanhanvien_Click(object sender, EventArgs e)
        {
            string sqlEdit = "UPDATE Nhanvien SET Tennv =@Tennv , Sdt = @Sdt , Diachi =@Diachi";
            SqlCommand cmd = new SqlCommand(sqlEdit, con);
            cmd.Parameters.AddWithValue("Manv", textBox_mnv.Text);
            cmd.Parameters.AddWithValue("Tennv", textBox_tnv.Text);
            cmd.Parameters.AddWithValue("Sdt", textBox_sdtnhanvien.Text);
            cmd.Parameters.AddWithValue("Diachi", textBox_diachinhanvien.Text);
            cmd.ExecuteNonQuery();
            Hienthi();
        }

        private void button_xoanhanvien_Click(object sender, EventArgs e)
        {
            string sqlDELETE = "DELETE FROM Nhanvien WHERE Manv =@Manv ";
            SqlCommand cmd = new SqlCommand(sqlDELETE, con);
            cmd.Parameters.AddWithValue("Manv", textBox_mnv.Text);
            cmd.Parameters.AddWithValue("Tennv", textBox_tnv.Text);
            cmd.Parameters.AddWithValue("Sdt", textBox_sdtnhanvien.Text);
            cmd.Parameters.AddWithValue("Diachi", textBox_diachinhanvien.Text);
            cmd.ExecuteNonQuery();
            Hienthi();
        }

        private void button_timkiemnhanvien_Click(object sender, EventArgs e)
        {
            string sqlTmkiem = "SELECT *FROM Nhanvien  WHERE Manv =@Manv";
            SqlCommand cmd = new SqlCommand(sqlTmkiem, con);
            cmd.Parameters.AddWithValue("Manv", textBox_mnv.Text);
            cmd.Parameters.AddWithValue("Tennv", textBox_tnv.Text);
            cmd.Parameters.AddWithValue("Sdt", textBox_sdtnhanvien.Text);
            cmd.Parameters.AddWithValue("Diachi", textBox_diachinhanvien.Text);
            cmd.ExecuteNonQuery();
            SqlDataReader dr = cmd.ExecuteReader();//de doc 
            DataTable dt = new DataTable();
            dt.Load(dr);
            dataGridView_nhanvien.DataSource = dt;
        }

        private void Nhanvien_FormClosed(object sender, FormClosedEventArgs e)
        {
            con.Close();
        }

        private void dataGridView_nhanvien_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }

        private void button_thoatnhanvien_Click(object sender, EventArgs e)
        {
            this.Close();
            Thongtinmaytinh frm = new Thongtinmaytinh();
            frm.Show();
        }
    }
}
