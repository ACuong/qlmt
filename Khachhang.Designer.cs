﻿
namespace BTL.NET1
{
    partial class Khachhang
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.textBox_makhachhang = new System.Windows.Forms.TextBox();
            this.textBox_tenkhachhang = new System.Windows.Forms.TextBox();
            this.textBox_sdtkhachhang = new System.Windows.Forms.TextBox();
            this.textBox_diachikh = new System.Windows.Forms.TextBox();
            this.button_themkhachhang = new System.Windows.Forms.Button();
            this.button_suakhachhang = new System.Windows.Forms.Button();
            this.button_xoakhachhang = new System.Windows.Forms.Button();
            this.button_thoatkhachhang = new System.Windows.Forms.Button();
            this.label6 = new System.Windows.Forms.Label();
            this.textBox_timkiemkhachhang = new System.Windows.Forms.TextBox();
            this.button_tkkhachhang = new System.Windows.Forms.Button();
            this.dataGridView_khachhang = new System.Windows.Forms.DataGridView();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView_khachhang)).BeginInit();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.Color.Blue;
            this.label1.Location = new System.Drawing.Point(221, 9);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(291, 25);
            this.label1.TabIndex = 0;
            this.label1.Text = "THÔNG TIN KHÁCH HÀNG";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(24, 52);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(122, 19);
            this.label2.TabIndex = 1;
            this.label2.Text = "Mã khách hàng :";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label3.Location = new System.Drawing.Point(24, 110);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(124, 19);
            this.label3.TabIndex = 2;
            this.label3.Text = "Tên khách hàng :";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(24, 163);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(104, 19);
            this.label4.TabIndex = 3;
            this.label4.Text = "Số điện thoại :";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(24, 207);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(65, 19);
            this.label5.TabIndex = 4;
            this.label5.Text = "Địa chỉ :";
            // 
            // textBox_makhachhang
            // 
            this.textBox_makhachhang.Location = new System.Drawing.Point(153, 51);
            this.textBox_makhachhang.Name = "textBox_makhachhang";
            this.textBox_makhachhang.Size = new System.Drawing.Size(144, 20);
            this.textBox_makhachhang.TabIndex = 5;
            this.textBox_makhachhang.TextChanged += new System.EventHandler(this.textBox1_TextChanged);
            // 
            // textBox_tenkhachhang
            // 
            this.textBox_tenkhachhang.Location = new System.Drawing.Point(153, 107);
            this.textBox_tenkhachhang.Name = "textBox_tenkhachhang";
            this.textBox_tenkhachhang.Size = new System.Drawing.Size(144, 20);
            this.textBox_tenkhachhang.TabIndex = 6;
            // 
            // textBox_sdtkhachhang
            // 
            this.textBox_sdtkhachhang.Location = new System.Drawing.Point(153, 164);
            this.textBox_sdtkhachhang.Name = "textBox_sdtkhachhang";
            this.textBox_sdtkhachhang.Size = new System.Drawing.Size(144, 20);
            this.textBox_sdtkhachhang.TabIndex = 7;
            // 
            // textBox_diachikh
            // 
            this.textBox_diachikh.Location = new System.Drawing.Point(153, 206);
            this.textBox_diachikh.Name = "textBox_diachikh";
            this.textBox_diachikh.Size = new System.Drawing.Size(144, 20);
            this.textBox_diachikh.TabIndex = 8;
            // 
            // button_themkhachhang
            // 
            this.button_themkhachhang.Location = new System.Drawing.Point(344, 49);
            this.button_themkhachhang.Name = "button_themkhachhang";
            this.button_themkhachhang.Size = new System.Drawing.Size(75, 23);
            this.button_themkhachhang.TabIndex = 9;
            this.button_themkhachhang.Text = "Thêm ";
            this.button_themkhachhang.UseVisualStyleBackColor = true;
            this.button_themkhachhang.Click += new System.EventHandler(this.button_themkhachhang_Click);
            // 
            // button_suakhachhang
            // 
            this.button_suakhachhang.Location = new System.Drawing.Point(474, 49);
            this.button_suakhachhang.Name = "button_suakhachhang";
            this.button_suakhachhang.Size = new System.Drawing.Size(75, 23);
            this.button_suakhachhang.TabIndex = 10;
            this.button_suakhachhang.Text = "Sửa";
            this.button_suakhachhang.UseVisualStyleBackColor = true;
            this.button_suakhachhang.Click += new System.EventHandler(this.button_suakhachhang_Click);
            // 
            // button_xoakhachhang
            // 
            this.button_xoakhachhang.Location = new System.Drawing.Point(603, 48);
            this.button_xoakhachhang.Name = "button_xoakhachhang";
            this.button_xoakhachhang.Size = new System.Drawing.Size(75, 23);
            this.button_xoakhachhang.TabIndex = 11;
            this.button_xoakhachhang.Text = "Xóa";
            this.button_xoakhachhang.UseVisualStyleBackColor = true;
            this.button_xoakhachhang.Click += new System.EventHandler(this.button_xoakhachhang_Click);
            // 
            // button_thoatkhachhang
            // 
            this.button_thoatkhachhang.Location = new System.Drawing.Point(713, 48);
            this.button_thoatkhachhang.Name = "button_thoatkhachhang";
            this.button_thoatkhachhang.Size = new System.Drawing.Size(75, 23);
            this.button_thoatkhachhang.TabIndex = 12;
            this.button_thoatkhachhang.Text = "Thoát";
            this.button_thoatkhachhang.UseVisualStyleBackColor = true;
            this.button_thoatkhachhang.Click += new System.EventHandler(this.button_thoatkhachhang_Click);
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Times New Roman", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label6.Location = new System.Drawing.Point(454, 107);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(170, 22);
            this.label6.TabIndex = 13;
            this.label6.Text = "Thông tin tìm kiếm ";
            // 
            // textBox_timkiemkhachhang
            // 
            this.textBox_timkiemkhachhang.Location = new System.Drawing.Point(467, 143);
            this.textBox_timkiemkhachhang.Name = "textBox_timkiemkhachhang";
            this.textBox_timkiemkhachhang.Size = new System.Drawing.Size(141, 20);
            this.textBox_timkiemkhachhang.TabIndex = 14;
            // 
            // button_tkkhachhang
            // 
            this.button_tkkhachhang.Location = new System.Drawing.Point(499, 186);
            this.button_tkkhachhang.Name = "button_tkkhachhang";
            this.button_tkkhachhang.Size = new System.Drawing.Size(75, 23);
            this.button_tkkhachhang.TabIndex = 15;
            this.button_tkkhachhang.Text = "Tìm kiếm ";
            this.button_tkkhachhang.UseVisualStyleBackColor = true;
            this.button_tkkhachhang.Click += new System.EventHandler(this.button_tkkhachhang_Click);
            // 
            // dataGridView_khachhang
            // 
            this.dataGridView_khachhang.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView_khachhang.Location = new System.Drawing.Point(57, 258);
            this.dataGridView_khachhang.Name = "dataGridView_khachhang";
            this.dataGridView_khachhang.Size = new System.Drawing.Size(621, 150);
            this.dataGridView_khachhang.TabIndex = 16;
            this.dataGridView_khachhang.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dataGridView_khachhang_CellContentClick);
            // 
            // Khachhang
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.Cyan;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.dataGridView_khachhang);
            this.Controls.Add(this.button_tkkhachhang);
            this.Controls.Add(this.textBox_timkiemkhachhang);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.button_thoatkhachhang);
            this.Controls.Add(this.button_xoakhachhang);
            this.Controls.Add(this.button_suakhachhang);
            this.Controls.Add(this.button_themkhachhang);
            this.Controls.Add(this.textBox_diachikh);
            this.Controls.Add(this.textBox_sdtkhachhang);
            this.Controls.Add(this.textBox_tenkhachhang);
            this.Controls.Add(this.textBox_makhachhang);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Name = "Khachhang";
            this.Text = "Khachhang";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.Khachhang_FormClosing);
            this.Load += new System.EventHandler(this.Khachhang_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView_khachhang)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox textBox_makhachhang;
        private System.Windows.Forms.TextBox textBox_tenkhachhang;
        private System.Windows.Forms.TextBox textBox_sdtkhachhang;
        private System.Windows.Forms.TextBox textBox_diachikh;
        private System.Windows.Forms.Button button_themkhachhang;
        private System.Windows.Forms.Button button_suakhachhang;
        private System.Windows.Forms.Button button_xoakhachhang;
        private System.Windows.Forms.Button button_thoatkhachhang;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox textBox_timkiemkhachhang;
        private System.Windows.Forms.Button button_tkkhachhang;
        private System.Windows.Forms.DataGridView dataGridView_khachhang;
    }
}