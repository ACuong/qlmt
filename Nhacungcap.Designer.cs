﻿
namespace BTL.NET1
{
    partial class Nhacungcap
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.button1_them = new System.Windows.Forms.Button();
            this.button2_sua = new System.Windows.Forms.Button();
            this.button3_xoa = new System.Windows.Forms.Button();
            this.dataGridView_ncc = new System.Windows.Forms.DataGridView();
            this.textBox1_mancc = new System.Windows.Forms.TextBox();
            this.textBox2_tenncc = new System.Windows.Forms.TextBox();
            this.textBox3_sdt = new System.Windows.Forms.TextBox();
            this.textBox4_diachi = new System.Windows.Forms.TextBox();
            this.button1_thoat = new System.Windows.Forms.Button();
            this.label6 = new System.Windows.Forms.Label();
            this.button_timkiem = new System.Windows.Forms.Button();
            this.textBox1 = new System.Windows.Forms.TextBox();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView_ncc)).BeginInit();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Times New Roman", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label1.Location = new System.Drawing.Point(12, 53);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(161, 22);
            this.label1.TabIndex = 0;
            this.label1.Text = "Mã nhà cung cấp :";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Times New Roman", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(12, 96);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(165, 22);
            this.label2.TabIndex = 1;
            this.label2.Text = "Tên nhà cung cấp :";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Times New Roman", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.ForeColor = System.Drawing.Color.Blue;
            this.label3.Location = new System.Drawing.Point(284, 9);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(223, 24);
            this.label3.TabIndex = 2;
            this.label3.Text = "Thông tin nhà cung cấp ";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Times New Roman", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(12, 141);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(126, 22);
            this.label4.TabIndex = 3;
            this.label4.Text = "Số điện thoại :";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Times New Roman", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(12, 182);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(78, 22);
            this.label5.TabIndex = 4;
            this.label5.Text = "Địa chỉ :";
            // 
            // button1_them
            // 
            this.button1_them.Location = new System.Drawing.Point(345, 52);
            this.button1_them.Name = "button1_them";
            this.button1_them.Size = new System.Drawing.Size(75, 23);
            this.button1_them.TabIndex = 5;
            this.button1_them.Text = "Thêm ";
            this.button1_them.UseVisualStyleBackColor = true;
            this.button1_them.Click += new System.EventHandler(this.button1_them_Click);
            // 
            // button2_sua
            // 
            this.button2_sua.Location = new System.Drawing.Point(454, 52);
            this.button2_sua.Name = "button2_sua";
            this.button2_sua.Size = new System.Drawing.Size(75, 23);
            this.button2_sua.TabIndex = 6;
            this.button2_sua.Text = "Sửa";
            this.button2_sua.UseVisualStyleBackColor = true;
            this.button2_sua.Click += new System.EventHandler(this.button2_sua_Click);
            // 
            // button3_xoa
            // 
            this.button3_xoa.Location = new System.Drawing.Point(563, 52);
            this.button3_xoa.Name = "button3_xoa";
            this.button3_xoa.Size = new System.Drawing.Size(75, 23);
            this.button3_xoa.TabIndex = 7;
            this.button3_xoa.Text = "Xóa";
            this.button3_xoa.UseVisualStyleBackColor = true;
            this.button3_xoa.Click += new System.EventHandler(this.button3_xoa_Click);
            // 
            // dataGridView_ncc
            // 
            this.dataGridView_ncc.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView_ncc.Location = new System.Drawing.Point(16, 250);
            this.dataGridView_ncc.Name = "dataGridView_ncc";
            this.dataGridView_ncc.Size = new System.Drawing.Size(744, 171);
            this.dataGridView_ncc.TabIndex = 8;
            this.dataGridView_ncc.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dataGridView_ncc_CellContentClick);
            // 
            // textBox1_mancc
            // 
            this.textBox1_mancc.Location = new System.Drawing.Point(179, 56);
            this.textBox1_mancc.Name = "textBox1_mancc";
            this.textBox1_mancc.Size = new System.Drawing.Size(131, 20);
            this.textBox1_mancc.TabIndex = 9;
            // 
            // textBox2_tenncc
            // 
            this.textBox2_tenncc.Location = new System.Drawing.Point(179, 99);
            this.textBox2_tenncc.Name = "textBox2_tenncc";
            this.textBox2_tenncc.Size = new System.Drawing.Size(131, 20);
            this.textBox2_tenncc.TabIndex = 10;
            // 
            // textBox3_sdt
            // 
            this.textBox3_sdt.Location = new System.Drawing.Point(179, 144);
            this.textBox3_sdt.Name = "textBox3_sdt";
            this.textBox3_sdt.Size = new System.Drawing.Size(131, 20);
            this.textBox3_sdt.TabIndex = 11;
            // 
            // textBox4_diachi
            // 
            this.textBox4_diachi.Location = new System.Drawing.Point(179, 185);
            this.textBox4_diachi.Name = "textBox4_diachi";
            this.textBox4_diachi.Size = new System.Drawing.Size(133, 20);
            this.textBox4_diachi.TabIndex = 12;
            // 
            // button1_thoat
            // 
            this.button1_thoat.Location = new System.Drawing.Point(685, 52);
            this.button1_thoat.Name = "button1_thoat";
            this.button1_thoat.Size = new System.Drawing.Size(75, 23);
            this.button1_thoat.TabIndex = 13;
            this.button1_thoat.Text = "Thoát";
            this.button1_thoat.UseVisualStyleBackColor = true;
            this.button1_thoat.Click += new System.EventHandler(this.button1_thoat_Click);
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Times New Roman", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(469, 97);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(154, 22);
            this.label6.TabIndex = 14;
            this.label6.Text = "Nhập mã cần tìm ";
            // 
            // button_timkiem
            // 
            this.button_timkiem.Location = new System.Drawing.Point(501, 182);
            this.button_timkiem.Name = "button_timkiem";
            this.button_timkiem.Size = new System.Drawing.Size(84, 25);
            this.button_timkiem.TabIndex = 15;
            this.button_timkiem.Text = "Tìm kiếm ";
            this.button_timkiem.UseVisualStyleBackColor = true;
            this.button_timkiem.Click += new System.EventHandler(this.button_timkiem_Click);
            // 
            // textBox1
            // 
            this.textBox1.Location = new System.Drawing.Point(454, 141);
            this.textBox1.Name = "textBox1";
            this.textBox1.Size = new System.Drawing.Size(185, 20);
            this.textBox1.TabIndex = 16;
            // 
            // Nhacungcap
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.Aqua;
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.textBox1);
            this.Controls.Add(this.button_timkiem);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.button1_thoat);
            this.Controls.Add(this.textBox4_diachi);
            this.Controls.Add(this.textBox3_sdt);
            this.Controls.Add(this.textBox2_tenncc);
            this.Controls.Add(this.textBox1_mancc);
            this.Controls.Add(this.dataGridView_ncc);
            this.Controls.Add(this.button3_xoa);
            this.Controls.Add(this.button2_sua);
            this.Controls.Add(this.button1_them);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Name = "Nhacungcap";
            this.Text = "Nhacungcap";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.Nhacungcap_FormClosing);
            this.Load += new System.EventHandler(this.Nhacungcap_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView_ncc)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Button button1_them;
        private System.Windows.Forms.Button button2_sua;
        private System.Windows.Forms.Button button3_xoa;
        private System.Windows.Forms.DataGridView dataGridView_ncc;
        private System.Windows.Forms.TextBox textBox1_mancc;
        private System.Windows.Forms.TextBox textBox2_tenncc;
        private System.Windows.Forms.TextBox textBox3_sdt;
        private System.Windows.Forms.TextBox textBox4_diachi;
        private System.Windows.Forms.Button button1_thoat;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Button button_timkiem;
        private System.Windows.Forms.TextBox textBox1;
    }
}