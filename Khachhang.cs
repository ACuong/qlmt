﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace BTL.NET1
{
    public partial class Khachhang : Form
    {
        public Khachhang()
        {
            InitializeComponent();
        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {

        }
        SqlConnection con;
        private void Khachhang_Load(object sender, EventArgs e)
        {
            string conString = @"Data Source=(localdb)\ProjectsV13;Initial Catalog=BTL.NET1;Integrated Security=True";
            con = new SqlConnection(conString);// tao doi tuong                           
            con.Open();
            Hienthi();
        }

        public void Hienthi()
        {
            string sqlSELECT = "SELECT *FROM Khachhang";
            SqlCommand cmd = new SqlCommand(sqlSELECT, con);
            SqlDataReader dr = cmd.ExecuteReader();//de doc 
            DataTable dt = new DataTable();// tao ra doi tuong bang 
            dt.Load(dr);
            dataGridView_khachhang.DataSource = dt;
        }

        private void button_themkhachhang_Click(object sender, EventArgs e)
        {
            string sqlINSERT = "INSERT Khachhang VALUES(@Makh,@Tenkh,@Sdt,@Diachi)";
            SqlCommand cmd = new SqlCommand(sqlINSERT, con);
            cmd.Parameters.AddWithValue("Makh", textBox_makhachhang.Text);
            cmd.Parameters.AddWithValue("Tenkh", textBox_tenkhachhang.Text);
            cmd.Parameters.AddWithValue("Sdt", textBox_sdtkhachhang.Text);
            cmd.Parameters.AddWithValue("Diachi", textBox_diachikh.Text);
            cmd.ExecuteNonQuery();
            Hienthi();
        }

        private void button_suakhachhang_Click(object sender, EventArgs e)
        {
            string sqlEdit = "UPDATE Khachhang SET Tenkh =@Tenkh , Sdt = @Sdt , Diachi =@Diachi";//de sua duoc thi kh phai ton tai trong sql va de ton tai no mk thong qua ma no de tim kiem de sua
            SqlCommand cmd = new SqlCommand(sqlEdit, con);
            cmd.Parameters.AddWithValue("Makh", textBox_makhachhang.Text);
            cmd.Parameters.AddWithValue("Tenkh", textBox_tenkhachhang.Text);
            cmd.Parameters.AddWithValue("Sdt", textBox_sdtkhachhang.Text);
            cmd.Parameters.AddWithValue("Diachi", textBox_diachikh.Text);
            cmd.ExecuteNonQuery();
            Hienthi();
        }

        private void button_xoakhachhang_Click(object sender, EventArgs e)
        {
            string sqlDELETE = "DELETE FROM Khachhang WHERE Mancc =@Mancc , Tenkh =@Tenkh , Sdt =@Sdt , Diachi =@Diachi";
            SqlCommand cmd = new SqlCommand(sqlDELETE, con);
            cmd.Parameters.AddWithValue("Makh", textBox_makhachhang.Text);
            cmd.Parameters.AddWithValue("Tenkh", textBox_tenkhachhang.Text);
            cmd.Parameters.AddWithValue("Sdt", textBox_sdtkhachhang.Text);
            cmd.Parameters.AddWithValue("Diachi", textBox_diachikh.Text);
            cmd.ExecuteNonQuery();
            Hienthi();
        }

        private void button_tkkhachhang_Click(object sender, EventArgs e)
        {
            string sqlTmkiem = "SELECT *FROM Khachhang WHERE Makh =@Makh";
            SqlCommand cmd = new SqlCommand(sqlTmkiem, con);
            cmd.Parameters.AddWithValue("Mancc", textBox_timkiemkhachhang.Text);
            cmd.Parameters.AddWithValue("Tenncc", textBox_timkiemkhachhang.Text);
            cmd.Parameters.AddWithValue("Sdt", textBox_timkiemkhachhang.Text);
            cmd.Parameters.AddWithValue("Diachi", textBox_timkiemkhachhang.Text);
            cmd.ExecuteNonQuery();
            SqlDataReader dr = cmd.ExecuteReader();//de doc 
            DataTable dt = new DataTable();
            dt.Load(dr);
            dataGridView_khachhang.DataSource = dt;
        }

        private void dataGridView_khachhang_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }

        private void button_thoatkhachhang_Click(object sender, EventArgs e)
        {
            this.Close();
            Nhanvien frm = new Nhanvien();
            frm.Show();
        }

        private void Khachhang_FormClosing(object sender, FormClosingEventArgs e)
        {
            con.Close();
        }
    }
}
