﻿
namespace BTL.NET1
{
    partial class Dangnhap
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Dangnhap));
            this.label1_tendangnhap = new System.Windows.Forms.Label();
            this.label2_matkhau = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.button1_dangnhap = new System.Windows.Forms.Button();
            this.button2_thoat = new System.Windows.Forms.Button();
            this.textBox1_tendangnhap = new System.Windows.Forms.TextBox();
            this.textBox2_matkhau = new System.Windows.Forms.TextBox();
            this.SuspendLayout();
            // 
            // label1_tendangnhap
            // 
            this.label1_tendangnhap.AutoSize = true;
            this.label1_tendangnhap.Font = new System.Drawing.Font("Times New Roman", 21.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1_tendangnhap.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(192)))));
            this.label1_tendangnhap.Location = new System.Drawing.Point(84, 109);
            this.label1_tendangnhap.Name = "label1_tendangnhap";
            this.label1_tendangnhap.Size = new System.Drawing.Size(216, 32);
            this.label1_tendangnhap.TabIndex = 0;
            this.label1_tendangnhap.Text = "Tên đăng nhập :";
            // 
            // label2_matkhau
            // 
            this.label2_matkhau.AutoSize = true;
            this.label2_matkhau.Font = new System.Drawing.Font("Times New Roman", 21.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label2_matkhau.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(192)))));
            this.label2_matkhau.Location = new System.Drawing.Point(84, 162);
            this.label2_matkhau.Name = "label2_matkhau";
            this.label2_matkhau.Size = new System.Drawing.Size(152, 32);
            this.label2_matkhau.TabIndex = 1;
            this.label2_matkhau.Text = "Mật khẩu :";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Times New Roman", 24F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label3.ForeColor = System.Drawing.Color.Red;
            this.label3.Location = new System.Drawing.Point(178, 44);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(347, 36);
            this.label3.TabIndex = 2;
            this.label3.Text = "Cửa hàng bán máy tính ";
            // 
            // button1_dangnhap
            // 
            this.button1_dangnhap.Font = new System.Drawing.Font("Times New Roman", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button1_dangnhap.ForeColor = System.Drawing.Color.Black;
            this.button1_dangnhap.Location = new System.Drawing.Point(159, 256);
            this.button1_dangnhap.Name = "button1_dangnhap";
            this.button1_dangnhap.Size = new System.Drawing.Size(139, 39);
            this.button1_dangnhap.TabIndex = 3;
            this.button1_dangnhap.Text = "Đăng nhập ";
            this.button1_dangnhap.UseVisualStyleBackColor = true;
            this.button1_dangnhap.Click += new System.EventHandler(this.button1_dangnhap_Click);
            // 
            // button2_thoat
            // 
            this.button2_thoat.Font = new System.Drawing.Font("Times New Roman", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button2_thoat.Location = new System.Drawing.Point(434, 257);
            this.button2_thoat.Name = "button2_thoat";
            this.button2_thoat.Size = new System.Drawing.Size(129, 38);
            this.button2_thoat.TabIndex = 4;
            this.button2_thoat.Text = "Thoát ";
            this.button2_thoat.UseVisualStyleBackColor = true;
            this.button2_thoat.Click += new System.EventHandler(this.button2_thoat_Click);
            // 
            // textBox1_tendangnhap
            // 
            this.textBox1_tendangnhap.BackColor = System.Drawing.Color.White;
            this.textBox1_tendangnhap.Location = new System.Drawing.Point(363, 121);
            this.textBox1_tendangnhap.Name = "textBox1_tendangnhap";
            this.textBox1_tendangnhap.Size = new System.Drawing.Size(162, 20);
            this.textBox1_tendangnhap.TabIndex = 5;
            // 
            // textBox2_matkhau
            // 
            this.textBox2_matkhau.Location = new System.Drawing.Point(363, 162);
            this.textBox2_matkhau.Name = "textBox2_matkhau";
            this.textBox2_matkhau.PasswordChar = '*';
            this.textBox2_matkhau.Size = new System.Drawing.Size(162, 20);
            this.textBox2_matkhau.TabIndex = 6;
            // 
            // Dangnhap
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("$this.BackgroundImage")));
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.textBox2_matkhau);
            this.Controls.Add(this.textBox1_tendangnhap);
            this.Controls.Add(this.button2_thoat);
            this.Controls.Add(this.button1_dangnhap);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2_matkhau);
            this.Controls.Add(this.label1_tendangnhap);
            this.Name = "Dangnhap";
            this.Text = "Form1";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1_tendangnhap;
        private System.Windows.Forms.Label label2_matkhau;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Button button1_dangnhap;
        private System.Windows.Forms.Button button2_thoat;
        private System.Windows.Forms.TextBox textBox1_tendangnhap;
        private System.Windows.Forms.TextBox textBox2_matkhau;
    }
}

